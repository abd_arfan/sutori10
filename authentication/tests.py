from django.test import TestCase, Client
from django.urls import resolve, reverse

from .views import *

# Create your tests here.
class TestLogin(TestCase):
    def test_index_url_exists(self):
        response = Client().get(reverse('index'))
        self.assertEquals(response.status_code, 200)

    def test_index_using_index_template(self):
        response = Client().get(reverse('index'))
        self.assertTemplateUsed(response, 'index.html')

    def test_index_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_login_url_exists(self):
        response = Client().get(reverse('login'))
        self.assertEquals(response.status_code, 200)

    def test_login_using_login_template(self):
        response = Client().get(reverse('login'))
        self.assertTemplateUsed(response, 'login.html')

    def test_login_using_login_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login_request)
    
    def test_register_url_exists(self):
        response = Client().get(reverse('register'))
        self.assertEquals(response.status_code, 200)

    def test_register_using_register_template(self):
        response = Client().get(reverse('register'))
        self.assertTemplateUsed(response, 'register.html')

    def test_register_using_register_function(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register_request)
    
    def test_logout_url_exists(self):
        response = Client().get(reverse('logout'))
        self.assertEquals(response.status_code, 302)

    def test_logout_using_logout_function(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logout_request)
    
    def test_login_can_save_user(self):
        response = self.client.post('/login/',
            data={'username': "violetto", "password": "evergarden"})
        self.assertEqual(response.status_code, 200)
    
    def test_tambahJadwal_can_save_jadwal(self):
        response = self.client.post('/register/',
            data={'username': "violetto", "email": "violet@evergarden.com", "password1":"evergarden", "password2":"evergarden"})
        self.assertEqual(response.status_code, 200)

